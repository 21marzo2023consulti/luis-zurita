import java.util.Scanner;

public class Algoritmo1 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] marcas = { "Nike", "Adidas", "Puma", "Reebok", "Under Armour", "Bunky", "Venus", "Vans", "Fila",
        "Converse" };
    String resultado = "";

    System.out.println("Ingrese 5 numeros enteros positivos separados por comas: ");
    String valoresIngreados = sc.next();

    String[] strArr = valoresIngreados.split(",");
    Integer[] varoles = new Integer[5];
    if (strArr.length > 5) {
      System.out.println("El limite de numeros a ingresar es 5");
      return;
    }
    for (int i = 0; i < strArr.length; i++) {
      if (isNumero(strArr[i])) {
        varoles[i] = Integer.parseInt(strArr[i]);
      } else {
        System.out.println("El formato no es permitido");
        return;
      }
    }

    System.out.println("test");
  }

  public static boolean isNumero(String strNum) {
    if (strNum == null) {
      return false;
    }
    try {
      Integer numero = Integer.parseInt(strNum);
    } catch (NumberFormatException nfe) {
      return false;
    }
    return true;
  }
}